package com.classpath.kafkastreamsdemo.service;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.ValueMapper;

import java.util.Properties;

public class CapitalizeStreamsApp {

    public static void main(String[] args) throws InterruptedException {

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        Serde<String> stringSerde = Serdes.String();

        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "capitalization-app");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "52.66.6.115:9092");


        //building a topology
        streamsBuilder.stream("input-topic", Consumed.with(stringSerde, stringSerde))
                      .mapValues((ValueMapper<String, String>) String::toUpperCase)
                      .to("output-topic", Produced.with(stringSerde, stringSerde));

        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), properties );
        kafkaStreams.start();
        System.out.println("Started with Kafka streams");
        Thread.sleep(35_000);
        kafkaStreams.close();

    }
}
