package com.classpath.kafkastreamsdemo.service;

import com.classpath.kafkastreamsdemo.config.ApplicationConfig;
import com.github.javafaker.Faker;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;
import java.util.stream.IntStream;

public class ProducerClient {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        logger.info("Creating Kafka Producer...");
        Properties props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, ApplicationConfig.APP_ID);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ApplicationConfig.BOOTSTRAP_SERVER);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<Integer, String> producer = new KafkaProducer<>(props);

        logger.info("Start sending messages...");


        IntStream.range(0,1000).forEach(index -> {
            String message = new Faker().chuckNorris().fact().toLowerCase();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            producer.send(new ProducerRecord<>(ApplicationConfig.TOPIC, index, message));
            logger.info("Successfully sent message to Broker:: "+ message);
        });
        logger.info("Finished - Closing Kafka Producer.");
        producer.close();
    }
}
