package com.classpath.kafkastreamsdemo.config;

public class ApplicationConfig {
    public static final String APP_ID="mock-producer";
    public static final String BOOTSTRAP_SERVER="52.66.6.115:9092";
    public static final String TOPIC="input-topic";

}
